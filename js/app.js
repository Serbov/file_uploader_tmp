/*
 * jQuery File Upload Plugin Angular JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* jshint nomen:false */
/* global window, angular */

(function () {
    'use strict';
    
    angular.module('app', [
        'blueimp.fileupload'
    ]);

    function FileUploaderController ($scope) {
        
        $scope.options = {
            maxFileSize: 111999000,
            acceptFileTypes: /(\.|\/)(MOV|MPG|AVI|FLV|F4V|MP4|M4V|ASF|WMV|VOB|MOD|3GP|MKV|DIVX|XVID|WEBM)$/i,
    
            formData: {'api_password': 'f492683b9480534af54964ba3cda1e1ff2f9ed4093e995bd396d8ee5799ddbc2'},
            url: 'https://upload.wistia.com/',                    
            done : function (e, data) {
                data.files[0].wistiaHashedId = data.result.hashed_id;
            }
        };
    
    }

    angular.module('app').component('fileUploader', {
        templateUrl: 'fileUploader.html',
        controller : FileUploaderController
    });


   
})();
